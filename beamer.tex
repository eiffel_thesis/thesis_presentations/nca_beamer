\newif \ifshare
% \sharetrue % Comment this if you want animation
\ifshare % "Share mode" without animation.
\documentclass[table, trans]{beamer}
\else % "Presentation mode" with animation.
\documentclass[table]{beamer}
\fi
\usepackage[utf8]{inputenc}
\usepackage{default}
\usepackage[english]{babel}
\usepackage{color}
\usepackage{siunitx}
\usepackage{pgfplots}
\usepackage{algpseudocode}
\usepackage{rotating}
\usepackage{bytefield}
\usepackage{graphicx}
\usepackage{diagbox}
\usepackage{subfig}
\usepackage{pifont}

\let\pgfmathMod=\pgfmathmod\relax

\sisetup{binary-units=true}

\graphicspath{{vectors/EPS/}{vectors/PDF/}}

\usetheme{Rochester}

\definecolor{struct}{HTML}{03a9f4}
\definecolor{alert}{HTML}{f44336}
\definecolor{example}{HTML}{aeea00}
\definecolor{struct2}{HTML}{8bc34a}
\definecolor{amber}{HTML}{ff9800}
\definecolor{ppink}{HTML}{e91e63}
\definecolor{violet_profond}{RGB}{103, 58, 183}
\definecolor{sarcelle}{RGB}{0, 150, 136}

\setbeamercolor{structure}{fg = struct}
\setbeamercolor{normal text}{fg = white, bg = black}
\setbeamercolor{example text}{fg = example}
\setbeamercolor{alerted text}{fg = alert}
\setbeamercolor{footline}{fg = white}

\setbeamertemplate{navigation symbols}

\setbeamerfont{page number in head/foot}{size=\small}
\setbeamertemplate{footline}[frame number]

\pgfkeys{/pgf/number format/1000 sep=}

\title{Highlighting container memory consolidation problem in Linux}
\author{Francis Laniel\inst{1}\\ \small{Damien Carver\inst{1}, Julien Sopena\inst{1}, Franck Wajsburt\inst{2}, Jonathan Lejeune\inst{1} and Marc Shapiro\inst{1}}}
\institute{\inst{1} Sorbonne Université, LIP6, INRIA\\ \inst{2} Sorbonne Université, LIP6\\ \includegraphics[scale = .3]{SU.pdf}}
\date{September 2019}
% \logo{\includegraphics[scale = .07]{LIP6.eps}}

% This need the pifont package.
% Information was taken from: https://tex.stackexchange.com/a/42620.
\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

\begin{document}
	\setbeamertemplate{caption}{\raggedright\insertcaption\par}

	\maketitle

	\begin{frame}
		\frametitle{Memory consolidation and memory isolation}

		\begin{figure}
			\centering

			\begin{columns}
				\begin{column}{.3\textwidth}<1->
					\centering

					\textbf{Processes share all memory}

					\includegraphics[scale = .15]{mem-fig1.pdf}
				\end{column}
				\begin{column}{.3\textwidth}<2->
					\centering

					\textbf{Memory of a container is bounded\footnotemark}

					\includegraphics[scale = .15]{mem-fig3.pdf}
				\end{column}
			\end{columns}

			\begin{itemize}
				\item<3-> Deployment: software packaging, \texttt{docker hub} \cite{docker_docker_nodate}.
				\item<3-> Security: \texttt{namespace}, capabilities \cite{kerrisk_lce_2012}.
				\item<3-> \textbf{Ressource management: \texttt{cgroups} \cite{hiroyu_cgroup_2008}}.
			\end{itemize}

			\begin{description}
				\item<4->[Memory consolidation:] Take unused memory from one container to give it to another one so it can increase its performance.
				\item<4->[Memory isolation:] Ensure a given amount of memory for each container.
			\end{description}
		\end{figure}

		\footnotetext[1]{\cite{ovh_ovh_nodate, alibaba_alibaba_nodate, amazon_amazon_nodate, microsoft_microsoft_nodate}}
	\end{frame}

	\begin{frame}
		\frametitle{Experiment}
		\framesubtitle{Does Linux ensure memory consolidation \textbf{and} memory isolation for containers?}

		Experimental settings:
		\begin{itemize}
			\item Two containers A and B.
			\item Both A and B require 4GB of memory available.
			\item But there is only 3GB of memory available.
			\item A and B alternate between high and low activity, at different time.
		\end{itemize}

		\only<2>{\centering\textbf{Dummy curves}}
		\begin{columns}
			\begin{column}{.5\textwidth}<2>
				\centering

				\begin{tikzpicture}[scale = .5]
					\begin{axis}[ymax = 1100, error bars/y dir = both, xlabel = Time (second), ylabel = Transactions per second, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
						\addplot[example] coordinates {(0, 800) (1080, 800)};
						\addlegendentry{Reference}
						\addplot[white] coordinates {(0, 200) (1080, 200)};
						\addlegendentry{200}
						\addplot[struct, thick] coordinates {(0, 700) (180, 700) (360, 700) (540, 400) (720, 400) (900, 400)};
						\addlegendentry{A}
						\addplot[amber, thick] coordinates {(0, 600) (180, 400) (360, 600) (540, 400) (720, 600) (900, 600) (1080, 600)};
						\addlegendentry{B}

						\node[alert] at (axis cs: 90, 880) {\xmark};
						\node[alert] at (axis cs: 270, 880) {\xmark};
						\node[alert] at (axis cs: 450, 880) {\xmark};
						\node[example] at (axis cs: 630, 880) {\cmark};
						\node[alert] at (axis cs: 810, 880) {\xmark};
						\node[example] at (axis cs: 990, 880) {\cmark};

						\node[amber] at (axis cs: 90, 950) {\textbf{high}};
						\node[amber] at (axis cs: 270, 950) {\textbf{low}};
						\node[amber] at (axis cs: 450, 950) {\textbf{high}};
						\node[amber] at (axis cs: 630, 950) {\textbf{low}};
						\node[amber] at (axis cs: 810, 950) {\textbf{high}};
						\node[amber] at (axis cs: 990, 950) {\textbf{high}};

						\node[struct] at (axis cs: 90, 1000) {\textbf{high}};
						\node[struct] at (axis cs: 270, 1000) {\textbf{high}};
						\node[struct] at (axis cs: 450, 1000) {\textbf{high}};
						\node[struct] at (axis cs: 630, 1000) {\textbf{low}};
						\node[struct] at (axis cs: 810, 1000) {\textbf{low}};
						\node[struct] at (axis cs: 990, 1000) {\textbf{none}};
					\end{axis}
				\end{tikzpicture}
			\end{column}

			\begin{column}{.5\textwidth}<2>
				\centering

				\begin{tikzpicture}[scale = .5]
					\begin{axis}[error bars/y dir = both, xlabel = Time (second), ylabel = Memory (byte), legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
						\addplot[example] coordinates {(0, 2.8 * 10^9) (1080, 2.8 * 10^9)};
						\addlegendentry{Reference}
						\addplot[struct, thick] coordinates {(0, 1.4 * 10^9) (180, 1.6 * 10^9) (360, 1.4 * 10^9) (540, 1.4 * 10^9) (720, 1.2 * 10^9) (900, 1.2 * 10^9)};
						\addlegendentry{A}
						\addplot[amber, thick] coordinates {(0, 1.4 * 10^9) (180, 1.2 * 10^9) (360, 1.4 * 10^9) (540, 1.4 * 10^9) (720, 1.6 * 10^9) (900, 1.2 * 10^9) (1080, 1.7 * 10^9)};
						\addlegendentry{B}

						\node[amber] at (axis cs: 90, 2.5 * 10^9) {\textbf{high}};
						\node[amber] at (axis cs: 270, 2.5 * 10^9) {\textbf{low}};
						\node[amber] at (axis cs: 450, 2.5 * 10^9) {\textbf{high}};
						\node[amber] at (axis cs: 630, 2.5 * 10^9) {\textbf{low}};
						\node[amber] at (axis cs: 810, 2.5 * 10^9) {\textbf{high}};
						\node[amber] at (axis cs: 990, 2.5 * 10^9) {\textbf{high}};

						\node[struct] at (axis cs: 90, 2.7 * 10^9) {\textbf{high}};
						\node[struct] at (axis cs: 270, 2.7 * 10^9) {\textbf{high}};
						\node[struct] at (axis cs: 450, 2.7 * 10^9) {\textbf{high}};
						\node[struct] at (axis cs: 630, 2.7 * 10^9) {\textbf{low}};
						\node[struct] at (axis cs: 810, 2.7 * 10^9) {\textbf{low}};
						\node[struct] at (axis cs: 990, 2.7 * 10^9) {\textbf{none}};
					\end{axis}
				\end{tikzpicture}
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}
		\frametitle{Experiments}
		\framesubtitle{Throughputs and memory footprints}

		\begin{columns}
			\begin{column}{.5\textwidth}
				\centering

				\textbf{No limits}

				\begin{figure}[H]
					\resizebox{\textwidth}{.4\textheight}{
						\begin{tikzpicture}
							\begin{axis}[ymax = 1100, error bars/y dir = both, xlabel = Time (seconds), ylabel = Transactions per second, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
								\addplot[example] coordinates {(0, 800) (1080, 800)};
								\addlegendentry{Reference}
								\addplot[white] coordinates {(0, 200) (1080, 200)};
								\addlegendentry{200}
								\addplot[struct] table [x = time, y = transactions_avg, y error = transactions_std, col sep = semicolon]{CSV/default/transactions_A.csv};
								\addlegendentry{A}
								\addplot[amber] table [x = time, y = transactions_avg, y error = transactions_std, col sep = semicolon]{CSV/default/transactions_B.csv};
								\addlegendentry{B}

								\node[alert] at (axis cs: 90, 880) {\xmark};
								\node[alert] at (axis cs: 270, 880) {\xmark};
								\node[alert] at (axis cs: 450, 880) {\xmark};
								\node[example] at (axis cs: 630, 880) {\cmark};
								\node[alert] at (axis cs: 810, 880) {\xmark};
								\node[example] at (axis cs: 990, 880) {\cmark};

								\node[amber] at (axis cs: 90, 950) {\textbf{high}};
								\node[amber] at (axis cs: 270, 950) {\textbf{low}};
								\node[amber] at (axis cs: 450, 950) {\textbf{high}};
								\node[amber] at (axis cs: 630, 950) {\textbf{low}};
								\node[amber] at (axis cs: 810, 950) {\textbf{high}};
								\node[amber] at (axis cs: 990, 950) {\textbf{high}};

								\node[struct] at (axis cs: 90, 1000) {\textbf{high}};
								\node[struct] at (axis cs: 270, 1000) {\textbf{high}};
								\node[struct] at (axis cs: 450, 1000) {\textbf{high}};
								\node[struct] at (axis cs: 630, 1000) {\textbf{low}};
								\node[struct] at (axis cs: 810, 1000) {\textbf{low}};
								\node[struct] at (axis cs: 990, 1000) {\textbf{none}};
							\end{axis}
						\end{tikzpicture}
					}
					\resizebox{\textwidth}{.4\textheight}{
						\begin{tikzpicture}
							\begin{axis}[error bars/y dir = both, xlabel = Time (seconds), ylabel = Memory (bytes), legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
								\addplot[example] coordinates {(0, 2.8 * 10^9) (1080, 2.8 * 10^9)};
								\addlegendentry{Reference}
								\addplot[struct] table [x = iteration, y = usage_avg, y error = usage_std, col sep = semicolon]{CSV/default/stats_A.csv};
								\addlegendentry{A}
								\addplot[amber] table [x = iteration, y = usage_avg, y error = usage_std, col sep = semicolon]{CSV/default/stats_B.csv};
								\addlegendentry{B}

								\node[amber] at (axis cs: 90, 2.5 * 10^9) {\textbf{high}};
								\node[amber] at (axis cs: 270, 2.5 * 10^9) {\textbf{low}};
								\node[amber] at (axis cs: 450, 2.5 * 10^9) {\textbf{high}};
								\node[amber] at (axis cs: 630, 2.5 * 10^9) {\textbf{low}};
								\node[amber] at (axis cs: 810, 2.5 * 10^9) {\textbf{high}};
								\node[amber] at (axis cs: 990, 2.5 * 10^9) {\textbf{high}};

								\node[struct] at (axis cs: 90, 2.7 * 10^9) {\textbf{high}};
								\node[struct] at (axis cs: 270, 2.7 * 10^9) {\textbf{high}};
								\node[struct] at (axis cs: 450, 2.7 * 10^9) {\textbf{high}};
								\node[struct] at (axis cs: 630, 2.7 * 10^9) {\textbf{low}};
								\node[struct] at (axis cs: 810, 2.7 * 10^9) {\textbf{low}};
								\node[struct] at (axis cs: 990, 2.7 * 10^9) {\textbf{none}};
							\end{axis}
						\end{tikzpicture}
					}
				\end{figure}
			\end{column}

			\begin{column}{.5\textwidth}<2>
				\centering

				\textbf{\texttt{Soft} limits}

				\begin{figure}[H]
					\resizebox{\textwidth}{.4\textheight}{
						\begin{tikzpicture}[scale = .5]
							\begin{axis}[ymax = 1100, error bars/y dir = both, xlabel = Time (seconds), ylabel = Transactions per second, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
								\addplot[example] coordinates {(0, 800) (1080, 800)};
								\addlegendentry{Reference}
								\addplot[white] coordinates {(0, 200) (1080, 200)};
								\addlegendentry{200}
								\addplot[struct] table [x = time, y = transactions_avg, y error = transactions_std, col sep = semicolon]{CSV/soft/transactions_A.csv};
								\addlegendentry{A}
								\addplot[amber] table [x = time, y = transactions_avg, y error = transactions_std, col sep = semicolon]{CSV/soft/transactions_B.csv};
								\addlegendentry{B}

								\node[example] at (axis cs: 90, 880) {\cmark};
								\node[alert] at (axis cs: 270, 880) {\xmark};
								\node[example] at (axis cs: 450, 880) {\cmark};
								\node[example] at (axis cs: 630, 880) {\cmark};
								\node[alert] at (axis cs: 810, 880) {\xmark};
								\node[example] at (axis cs: 990, 880) {\cmark};

								\node[amber] at (axis cs: 90, 950) {\textbf{high}};
								\node[amber] at (axis cs: 270, 950) {\textbf{low}};
								\node[amber] at (axis cs: 450, 950) {\textbf{high}};
								\node[amber] at (axis cs: 630, 950) {\textbf{low}};
								\node[amber] at (axis cs: 810, 950) {\textbf{high}};
								\node[amber] at (axis cs: 990, 950) {\textbf{high}};

								\node[struct] at (axis cs: 90, 1000) {\textbf{high}};
								\node[struct] at (axis cs: 270, 1000) {\textbf{high}};
								\node[struct] at (axis cs: 450, 1000) {\textbf{high}};
								\node[struct] at (axis cs: 630, 1000) {\textbf{low}};
								\node[struct] at (axis cs: 810, 1000) {\textbf{low}};
								\node[struct] at (axis cs: 990, 1000) {\textbf{none}};
							\end{axis}
						\end{tikzpicture}
					}
					\resizebox{\textwidth}{.4\textheight}{
						\begin{tikzpicture}[scale = .5]
							\begin{axis}[error bars/y dir = both, xlabel = Time (seconds), ylabel = Memory (bytes), legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1, fill = none}]
								\addplot[ppink] coordinates {(0, 18 * 10^8) (1080, 18 * 10^8)};
								\addlegendentry{A's \texttt{soft} limit}
								\addplot[ppink, dotted] coordinates {(0, 10 * 10^8) (1080, 10 * 10^8)};
								\addlegendentry{B's \texttt{soft} limit}
								\addplot[example] coordinates {(0, 2.8 * 10^9) (1080, 2.8 * 10^9)};
								\addplot[struct] table [x = iteration, y = usage_avg, y error = usage_std, col sep = semicolon]{CSV/soft/stats_A.csv};
								\addplot[amber] table [x = iteration, y = usage_avg, y error = usage_std, col sep = semicolon]{CSV/soft/stats_B.csv};

								\node[amber] at (axis cs: 90, 2.5 * 10^9) {\textbf{high}};
								\node[amber] at (axis cs: 270, 2.5 * 10^9) {\textbf{low}};
								\node[amber] at (axis cs: 450, 2.5 * 10^9) {\textbf{high}};
								\node[amber] at (axis cs: 630, 2.5 * 10^9) {\textbf{low}};
								\node[amber] at (axis cs: 810, 2.5 * 10^9) {\textbf{high}};
								\node[amber] at (axis cs: 990, 2.5 * 10^9) {\textbf{high}};

								\node[struct] at (axis cs: 90, 2.7 * 10^9) {\textbf{high}};
								\node[struct] at (axis cs: 270, 2.7 * 10^9) {\textbf{high}};
								\node[struct] at (axis cs: 450, 2.7 * 10^9) {\textbf{high}};
								\node[struct] at (axis cs: 630, 2.7 * 10^9) {\textbf{low}};
								\node[struct] at (axis cs: 810, 2.7 * 10^9) {\textbf{low}};
								\node[struct] at (axis cs: 990, 2.7 * 10^9) {\textbf{none}};
							\end{axis}
						\end{tikzpicture}
					}
				\end{figure}
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}
		\frametitle{Conclusion}
		\framesubtitle{Observation}

		\begin{scriptsize}
			\begin{table}
				\centering

				\begin{tabular}{|l|c|c|}
					\hline
					Mechanism used & Memory consolidation & Memory performance isolation\\
					\hline
					no limits & weak & no\\
					\hline
					\texttt{max} limit & no & yes\\
					\hline
					\texttt{soft} limit & no & yes\\
					\hline
				\end{tabular}
			\end{table}
		\end{scriptsize}

		\begin{itemize}
			\item Containers permit:
				\begin{itemize}
					\item Resource limitation (\texttt{cgroups}).
					\item Application isolation (\texttt{namespace}).
				\end{itemize}
			\item They offer memory isolation.
			\item But memory consolidation is imperfect.
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Future work}
		\framesubtitle{Memory consolidation with feedback from application}

		\begin{figure}[H]
			\centering

			\includegraphics<1>[scale = .5]{performance-fig1.pdf}
			\includegraphics<2>[scale = .5]{performance-fig2.pdf}
			\includegraphics<3>[scale = .5]{performance-fig3.pdf}
			\includegraphics<4>[scale = .5]{performance-fig4.pdf}
			\includegraphics<5>[scale = .5]{performance-fig5.pdf}
			\includegraphics<6>[scale = .5]{performance-fig6.pdf}
			\includegraphics<7>[scale = .5]{performance-fig7.pdf}
			\includegraphics<8>[scale = .5]{performance-fig8.pdf}
			\includegraphics<9>[scale = .5]{performance-fig9.pdf}
			\includegraphics<10>[scale = .5]{performance-fig10.pdf}
		\end{figure}
	\end{frame}

	\begin{frame}[allowframebreaks, noframenumbering]
		\frametitle{Bibliography}
		\setbeamertemplate{bibliography item}[text]

		\begin{scriptsize}
			\bibliographystyle{acm}
			\bibliography{bibliography}

			\bigskip
			Pictures designed by \href{https://www.flaticon.com/authors/smashicons}{Smashicons}, \href{https://www.flaticon.com/authors/good-ware}{Good Ware}, \href{https://icon54.com/}{Icons mind}, \href{https://www.flaticon.com/authors/simpleicon}{SimpleIcon}, \href{https://www.sketchappsources.com/contributor/solarez}{Norman Posselt} and \href{http://www.freepik.com}{Freepik} from \href{http://www.flaticon.com}{Flaticon}.
		\end{scriptsize}
	\end{frame}

	\begin{frame}[noframenumbering]
		\frametitle{Between processes and containers: the virtual machine}

		\begin{figure}
			\centering

			\textbf{Memory of a VM is bounded too}

			\includegraphics[scale = .15]{mem-fig2.pdf}
		\end{figure}
	\end{frame}
\end{document}